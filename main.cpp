#include <iostream>

using namespace std;

const int row = 10, column = 2;

int matrix[row][column] = {
        //  f1          f2
        {3,  2},
        {5,  6},
        {5,  3},
        {8,  4},
        {10, 1},
        {3,  8},
        {6,  4},
        {1,  10},
        {9,  2},
        {3,  9},
};
int i, j, k;
int matrix_f1[row] = {}, matrix_f2[row] = {};

int main() {
    setlocale(LC_ALL, "Russian");
    //Перепишем в одномерные массивы значения каждого из критериев
    for (i = 0; i < row; i++) {
        for (j = 0; j < column - 1; j++) {
            matrix_f1[i] = matrix[i][j];
            matrix_f2[i] = matrix[i][j + 1];
        }
    }

    cout << "F1" << endl;
    for (i = 0; i < row; i++) {
        cout << matrix_f1[i] << " ";
    }
    cout << endl;

    cout << "F2" << endl;
    for (i = 0; i < row; i++) {
        cout << matrix_f2[i] << " ";
    }
    cout << endl << endl;

    //Убираем доминирование без эквивалентности
    for (k = 0; k < row; k++) {
        for (i = 1; i < row; i++) {
            if (matrix_f1[k] < matrix_f1[i] && matrix_f2[k] < matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[k][j] = 0;
                }
            } else if (matrix_f1[k] > matrix_f1[i] && matrix_f2[k] > matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    //Убираем эквивалентности и доминирования
    for (k = 0; k < row; k++) {
        for (i = 1; i < row; i++) {
            if (matrix_f1[k] == matrix_f1[i] && matrix_f2[k] < matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[k][j] = 0;
                }
            } else if (matrix_f1[k] == matrix_f1[i] && matrix_f2[k] > matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[i][j] = 0;
                }
            }

            if (matrix_f1[k] < matrix_f1[i] && matrix_f2[k] == matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[k][j] = 0;
                }
            } else if (matrix_f1[k] > matrix_f1[i] && matrix_f2[k] == matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[i][j] = 0;
                }
            }
            if (k < i && matrix_f1[k] == matrix_f1[i] && matrix_f2[k] == matrix_f2[i]) {
                for (j = 0; j < column; j++) {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    int count = 0;
    for (i = 0; i < row; i++) {
        for (j = 0; j < column; j++) {
            if (matrix[i][j] != 0) {
                count++;
            }
        }
    }
    int count_row = count / 2;

    //Создадим массив, содержащий Парето-границу
    int **Pareto = new int *[count_row];
    for (j = 0; j < count_row; j++)
        Pareto[j] = new int[column];

    k = 0;
    for (i = 0; i < row; i++) {
        for (j = 0; j < column - 1; j++) {
            if (matrix[i][j] != 0) {
                Pareto[k][j] = matrix[i][j];
                Pareto[k][j + 1] = matrix[i][j + 1];
                k++;
            }
        }
    }

    cout << "Паретто-граница" << endl;
    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column; j++) {
            cout << Pareto[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;

    int max_f1 = 0, max_f2 = 0;
    int f1max_f2[column] = {0, 0}, f2max_f1[column] = {0, 0};

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column - 1; j++) {
            if (Pareto[i][j] > max_f1) {
                max_f1 = Pareto[i][j];
                f1max_f2[j] = max_f1;
                f1max_f2[j + 1] = Pareto[i][j + 1];

            }
            if (Pareto[i][j + 1] > max_f2) {
                max_f2 = Pareto[i][j + 1];
                f2max_f1[j] = Pareto[i][j];
                f2max_f1[j + 1] = max_f2;
            }
        }
    }

    cout << "f1max; f2" << endl;
    for (i = 0; i < column; i++) {
        cout << f1max_f2[i] << " ";
    }
    cout << endl;

    cout << "f2max; f1" << endl;
    for (i = 0; i < column; i++) {
        cout << f2max_f1[i] << " ";
    }
    cout << endl;
    int *P1 = new int[count_row];
    int *P2 = new int[count_row];

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column - 1; j++) {
            P1[i] = Pareto[i][j];
            P2[i] = Pareto[i][j + 1];
        }
    }

    cout << endl;
    std::cout << "P1: ";
    for (i = 0; i < count_row; i++) {
        cout << P1[i] << " ";
    }
    cout << endl;

    std::cout << "P2: ";
    for (i = 0; i < count_row; i++) {
        cout << P2[i] << " ";
    }
    cout << endl;

    int *P11 = new int[count_row];
    int *P21 = new int[count_row];
    int *P12 = new int[count_row];
    int *P22 = new int[count_row];

    for (i = 0; i < count_row; i++) {
        P11[i] = P1[i];
        P12[i] = P1[i];
        P21[i] = P2[i];
        P22[i] = P2[i];
    }


    int **PX1 = new int *[count_row];
    for (j = 0; j < count_row; j++)
        PX1[j] = new int[column];

    int **PX2 = new int *[count_row];
    for (j = 0; j < count_row; j++)
        PX2[j] = new int[column];


    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column; j++) {
            PX1[i][j] = Pareto[i][j];
            PX2[i][j] = Pareto[i][j];
        }
    }

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column; j++) {
            cout << PX1[i][j];
        }
        cout << endl;
    }
    cout << endl;

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column; j++) {
            cout << PX2[i][j];
        }
        cout << endl;
    }
    cout << endl;

    int temp;
    for (i = 0; i < count_row; i++) {
        for (j = 0; j < count_row - 1; j++) {
            if (P21[j] < P21[j + 1]) {
                temp = P21[j];
                P21[j] = P21[j + 1];
                P21[j + 1] = temp;
            }
        }
    }

    cout << "P11" << endl;
    for (i = 0; i < count_row; i++) {
        cout << P11[i] << " " << endl;
    }
    cout << endl;

    cout << "P22" << endl;
    for (i = 0; i < count_row; i++) {
        cout << P22[i] << " " << endl;
    }
    cout << endl;

    cout << "P21" << endl;
    for (i = 0; i < count_row; i++) {
        cout << P21[i] << " " << endl;
    }
    cout << endl;

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < count_row - 1; j++) {
            if (P12[j] < P12[j + 1]) {
                temp = P12[j];
                P12[j] = P12[j + 1];
                P12[j + 1] = temp;
            }
        }
    }

    cout << "P12" << endl;
    for (i = 0; i < count_row; i++) {
        cout << P12[i] << " " << endl;
    }
    cout << endl;

    int temp_2;
    for (k = 0; k < count_row; k++) {
        for (i = 0; i < count_row - 1; i++) {
            for (j = 0; j < column - 1; j++) {
                if (PX1[i][j + 1] < PX1[i + 1][j + 1]) {
                    temp = PX1[i][j + 1];
                    temp_2 = PX1[i][j];
                    PX1[i][j + 1] = PX1[i + 1][j + 1];
                    PX1[i][j] = PX1[i + 1][j];
                    PX1[i + 1][j + 1] = temp;
                    PX1[i + 1][j] = temp_2;
                }
            }
        }
    }

    for (i = 0; i < count_row - 1; i++) {
        for (j = 0; j < column - 1; j++) {
            P11[i] = PX1[i][j + 1];
        }
    }

    for (k = 0; k < count_row; k++) {
        for (i = 0; i < count_row - 1; i++) {
            for (j = 0; j < column - 1; j++) {
                if (PX2[i][j] < PX2[i + 1][j]) {
                    temp = PX2[i][j];
                    temp_2 = PX2[i][j + 1];
                    PX2[i][j] = PX2[i + 1][j];
                    PX2[i + 1][j] = temp;
                    PX2[i][j + 1] = PX2[i + 1][j + 1];
                    PX2[i + 1][j + 1] = temp_2;
                }

            }
        }
    }

    for (i = 0; i < count_row; i++) {
        for (j = 0; j < column - 1; j++) {
            P22[i] = PX2[i][j];
        }
    }

    //УСТУПИ И ПРИРАСТИ
    int x[column] = {};
    for (int i = 0; i < count_row - 1; i++) {
        for (int j = 0; j < column - 1; j++) {
            if (PX1[i][j] == PX2[i][j] && PX1[i][j + 1] == PX2[i][j + 1]) {
                x[j] = PX1[i][j];
                x[j + 1] = PX1[i][j + 1];
            } else if (PX1[i + 1][j] == PX2[i + 1][j] && PX1[i + 1][j + 1] == PX2[i + 1][j + 1]) {
                x[j] = PX1[i + 1][j];
                x[j + 1] = PX1[i + 1][j + 1];
            } else if (PX1[i][j] == PX2[i + 1][j] && PX1[i][j + 1] == PX2[i + 1][j + 1]) {
                x[j] = PX1[i][j];
                x[j + 1] = PX1[i][j + 1];
            } else if (PX1[i + 1][j] == PX2[i][j] && PX1[i + 1][j + 1] == PX2[i + 1][j]) {
                x[j] = PX2[i][j];
                x[j + 1] = PX2[i][j + 1];
            }
        }
    }

    cout << "Most effective:" << endl;
    cout << "f1 = " << x[0] << ", f2 = " << x[1] << endl;
}
